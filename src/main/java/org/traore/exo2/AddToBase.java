package org.traore.exo2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class AddToBase {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test");

	File prof_instru = new File("File/professeurs-instruments.txt");
	File eleve = new File("File/eleves.txt");
	File instruments = new File("File/instruments.txt");
	File professeurs = new File("File/professeurs.txt");
	File eleves_instru = new File("File/eleves-instruments.txt");
	List<Instruments> instrument = new ArrayList<Instruments>();
	List<Professeurs> prof = new ArrayList<Professeurs>();
	List<Eleves> eleves = new ArrayList<Eleves>();

	public List<String> read(File file) throws FileNotFoundException {
		Reader rd = new FileReader(file);
		BufferedReader br = new BufferedReader(rd);
		List<String> listfinal = new ArrayList<String>();
		List<String> chaine = br.lines().map(s -> s.replaceAll("	", ";")).map(s -> s.replaceAll(" ", ";"))
				.filter(s -> !s.startsWith("#")).skip(1).collect(Collectors.toList());
		for (String s : chaine) {
			String finalchaine = "";
			String[] sp = s.split(";");
			for (String c : sp) {
				if (!c.isEmpty()) {
					finalchaine = finalchaine + c + ";";
				}
			}

			listfinal.add(finalchaine.substring(0, finalchaine.length() - 1));
		}

		return listfinal;

	}

	public List<Professeurs> StringToProf(File file) throws FileNotFoundException {
		List<String> chaine = this.read(professeurs);
		List<Professeurs> prof = new ArrayList<Professeurs>();
		for (String s : chaine) {
			String[] split = s.split(";");
			prof.add(new Professeurs(split[0], Integer.parseInt(split[1])));

		}

		return prof;

	}

	public List<Eleves> StringToEleves() throws FileNotFoundException {
		List<String> chaine = this.read(eleve);

		for (String s : chaine) {
			String[] split = s.split(";");
			eleves.add(new Eleves(split[0], Integer.parseInt(split[1])));

		}

		return eleves;

	}

	public List<Instruments> StringToInstruments(File file) throws FileNotFoundException {
		List<String> chaine = this.read(instruments);

		for (String s : chaine) {
			String[] split = s.split(";");
			instrument.add(new Instruments(split[0], Integer.parseInt(split[1]), Integer.parseInt(split[2]), split[3]));

		}

		return instrument;

	}

	

	public void remplirprof() throws FileNotFoundException {
		List<String> chaine = this.read(prof_instru);
		prof = this.StringToProf(professeurs);
		instrument = this.StringToInstruments(instruments);

		for (String s : chaine) {
			String[] split = s.split(";");
			for (Professeurs p : prof) {
				if (split[0].equals(p.getNom())) {
					for (Instruments i : instrument) {
						if (i.getNom().equals(split[1])) {
							p.setInstrument(i);
							i.setProfesseur(p);
						}
					}
				}
			}

		}
	}

	public void remplireleves() throws FileNotFoundException {
		List<String> chaine = this.read(eleves_instru);
		List<Instruments> instru = new ArrayList<Instruments>();
		eleves=this.StringToEleves();
	
		for (String s : chaine) {
			String[] split = s.split(";");
			for (Eleves eleve : eleves) {
			
				if (split[0].equals(eleve.getNom())) {
					for (Instruments ins : instrument) {

						for (int i = 1; i < split.length; i++) {
							if (ins.getNom().equals(split[i])) {
								
								eleve.getInstrument().add(ins);
								ins.getEleve().add(eleve);
								instru.add(ins);
							}
						}
					}
				}

				eleve.setInstrument(instru);
			}

		}

	}
	
	
	public void createInstruments(Instruments instrument) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(instrument);
		em.getTransaction().commit();

	}

	public void addInstruments() throws FileNotFoundException, ParseException {

		

		for (Instruments i : instrument)
			this.createInstruments(i);

	}
	
	public void createprof(Eleves e) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(e);
		em.getTransaction().commit();

	}

	public void addProf() throws FileNotFoundException, ParseException {

		

		for (Eleves eleve : eleves)
			this.createprof(eleve);

	}

}
