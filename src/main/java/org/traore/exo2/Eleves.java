package org.traore.exo2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity(name="Eleves") @Table(name="eleves", uniqueConstraints={
        @UniqueConstraint(name="nom", columnNames={"nom"})})
public class Eleves {
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	@Column(name="nom",nullable=false,length=40)
	private String nom;
	@Column(name="age",nullable=false)
	private int age;
	
	@ManyToMany(cascade={CascadeType.ALL})
	List<Instruments> instrument=new ArrayList<Instruments>() ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Instruments> getInstrument() {
		return instrument;
	}

	public void setInstrument(List<Instruments> instrument) {
		this.instrument = instrument;
	}

	public Eleves(String nom, int age) {
		
		this.nom = nom;
		this.age = age;
		
	}

	public Eleves() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Eleves [id=" + id + ", nom=" + nom + ", age=" + age +"]";
	}
	

	
	
	
	
	

}
