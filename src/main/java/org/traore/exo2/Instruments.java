package org.traore.exo2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name = "Instruments")
@Table(name = "instruments", uniqueConstraints = { @UniqueConstraint(name = "nom", columnNames = { "nom" }) })
public class Instruments {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;

	@Column(name="nom",nullable = false, length = 30)
	private String nom;
	@Column(name="prix",nullable = false, length = 11)
	private int prix;
	@Column(name="prix_cours",nullable = false, length = 11)
	private int prix_cours;
	@Column(name="location",nullable = false, length = 1)
	private String location;

	@ManyToMany(mappedBy = "instrument")
	private List<Eleves> eleve =new ArrayList<Eleves>();
	
	@OneToOne(cascade={CascadeType.ALL})
	private Professeurs professeur;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getPrix_cours() {
		return prix_cours;
	}

	public void setPrix_cours(int prix_cours) {
		this.prix_cours = prix_cours;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<Eleves> getEleve() {
		return eleve;
	}

	public void setEleve(List<Eleves> eleve) {
		this.eleve = eleve;
	}

	public Professeurs getProfesseur() {
		return professeur;
	}

	public void setProfesseur(Professeurs professeur) {
		this.professeur = professeur;
	}

	public Instruments() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Instruments [id=" + id + ", nom=" + nom + ", prix=" + prix + ", prix_cours=" + prix_cours
				+ ", location=" + location +  "]";
	}

	public Instruments(String nom, int prix, int prix_cours, String location) {
		
		this.nom = nom;
		this.prix = prix;
		this.prix_cours = prix_cours;
		this.location = location;
		
	}
	
	
	

}
