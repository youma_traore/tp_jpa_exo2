package org.traore.exo2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity(name="Professeurs") @Table(name="professeurs", uniqueConstraints={
        @UniqueConstraint(name="nom", columnNames={"nom"})})
public class Professeurs {
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	@Column(name="nom",nullable=false,length=40)
	private String nom;
	@Column(name="age",nullable=false)
	private int age;
	
	@OneToOne
	private Instruments instrument;

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	

	public Professeurs( String nom, int age) {
		this.nom = nom;
		this.age = age;
	}

	public Instruments getInstrument() {
		return instrument;
	}

	public void setInstrument(Instruments instrument) {
		this.instrument = instrument;
	}

	public Professeurs() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Professeurs [id=" + id + ", nom=" + nom + ", age=" + age + "]";
	}
	
	
	
	

}
